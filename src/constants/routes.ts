export const SIGN_UP = "/signup";
export const SIGN_IN = "/signin";
export const HOME = "/";
export const DASHBOARD = "/dashboard";
export const DASHBOARD_SUBROUTE = "/dashboard/:subroute";
