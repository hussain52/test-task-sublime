import * as React from "react";
import * as routes from "../../constants/routes";
import { auth } from "../../firebase";
import { CircularProgress } from "@material-ui/core";

interface IProps {
  email?: string;
  error?: any;
  history?: any;
  password?: string;
}

interface IState {
  email: string;
  error: any;
  password: string;
  progress: boolean;
}

export class SignInForm extends React.Component<IProps, IState> {
  private static INITIAL_STATE = {
    email: "",
    error: null,
    password: "",
    progress: false,
  };

  private static propKey(propertyName: string, value: any): object {
    return { [propertyName]: value };
  }

  constructor(props: IProps) {
    super(props);
    this.state = { ...SignInForm.INITIAL_STATE };
  }

  public onSubmit = (event: any) => {
    const { email, password } = this.state;

    const { history } = this.props;
    this.setState(SignInForm.propKey("progress", true));

    auth
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        history.push(routes.HOME);
      })
      .catch((error) => {
        this.setState(SignInForm.propKey("error", error));
        this.setState(SignInForm.propKey("progress", false));
      });

    event.preventDefault();
  };

  public render() {
    const { email, password, error, progress } = this.state;

    return (
      <form onSubmit={(event) => this.onSubmit(event)}>
        <input type="hidden" value="prayer" />
        <input
          className="input"
          value={email}
          onChange={(event) => this.setStateWithEvent(event, "email")}
          type="text"
          placeholder="Email Address"
          spellCheck={false}
        />
        <input
          className="input"
          spellCheck={false}
          value={password}
          onChange={(event) => this.setStateWithEvent(event, "password")}
          type="password"
          placeholder="Password"
        />
        <button className="button" type="submit">
          {progress ? (
            <CircularProgress style={{ color: "#fff" }} size={20} />
          ) : (
            "Sign In"
          )}
        </button>

        {error && (
          <p style={{ color: "#d63031", marginTop: 15 }}>{error.message}</p>
        )}
      </form>
    );
  }

  private setStateWithEvent(event: any, columnType: string): void {
    this.setState(SignInForm.propKey(columnType, (event.target as any).value));
  }
}
