import * as React from "react";
import { Link } from "react-router-dom";
import { SignInForm } from "./SignInForm";
import { withoutAuthorization } from "../Session/withoutAuthorization";

const SignInComponent = ({ history }: { [key: string]: any }) => (
  <div>
    <div className="login_overlay"></div>
    <div className="login_container">
      <h2>Sign In</h2>
      <SignInForm history={history} />
      <div className="signup_link">
        <Link to="/signup">Don't have an account?</Link>
      </div>
    </div>
  </div>
);

const SignIn = withoutAuthorization(SignInComponent);
export default SignIn;
