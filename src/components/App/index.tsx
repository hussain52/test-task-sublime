import * as React from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import * as routes from "../../constants/routes";
import { Dashboard } from "../Dashboard";
import Landing from "../Landing";
import { withAuthentication } from "../Session/withAuthentication";
import SignIn from "../SignIn";
import SignUp from "../SignUp";

class AppComponent extends React.Component {
  public render() {
    return (
      <Router forceRefresh={true}>
        <div>
          <Switch>
            <Route exact={true} path={routes.HOME} component={Landing} />
            <Route exact={true} path={routes.SIGN_IN} component={SignIn} />
            <Route exact={true} path={routes.SIGN_UP} component={SignUp} />
            <Route exact={true} path={routes.DASHBOARD} component={Dashboard} />
            <Route
              exact={true}
              path={routes.DASHBOARD_SUBROUTE}
              component={Dashboard}
            />
          </Switch>
        </div>
      </Router>
    );
  }
}

export const App = withAuthentication(AppComponent);
