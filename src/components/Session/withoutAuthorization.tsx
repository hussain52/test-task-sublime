import * as React from "react";
import { connect } from "react-redux";
import { withRouter, Redirect } from "react-router-dom";
import { compose } from "recompose";
// import * as routes from "../../constants/routes";
// import { firebase } from "../../firebase";

export const withoutAuthorization = (Component: any) => {
  class WithoutAuthorization extends React.Component<any, any> {
    public render() {
      if (this.props.authUser !== "NO_USER") {
        return this.props.authUser ? (
          <Redirect to="/dashboard" />
        ) : (
          this.props.authUser === null && <Component />
        );
      } else return null;
    }
  }

  const mapStateToProps = (state: any) => ({
    authUser: state.sessionState.authUser,
  });

  return compose(withRouter, connect(mapStateToProps))(WithoutAuthorization);
};
