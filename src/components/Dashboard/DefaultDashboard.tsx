import React from "react";
import Placeholder from "./Placeholder";

const DefaultDashboard = () => {
  return (
    <div>
      <Placeholder>Default Route</Placeholder>
    </div>
  );
};

export default DefaultDashboard;
