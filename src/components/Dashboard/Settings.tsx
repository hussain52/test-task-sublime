import React from "react";
import Placeholder from "./Placeholder";

const Settings = () => {
  return (
    <div>
      <Placeholder>Settings</Placeholder>
    </div>
  );
};

export default Settings;
