import React from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import DashboardHome from "./DashboardHome";
import Settings from "./Settings";
import UserList from "./UserList";
import Analytics from "./Analytics";
import DefaultDashboard from "./DefaultDashboard";

interface IProps {
  match?: any;
}

const DashboardItemComponent = ({ match }: IProps & RouteComponentProps) => {
  const renderItem = () => {
    switch (match.params.subroute || "") {
      case "":
        return <DashboardHome />;
      case "users":
        return <UserList />;
      case "settings":
        return <Settings />;
      case "analytics":
        return <Analytics />;
      default:
        return <DefaultDashboard />;
    }
  };

  return <div className="dashboard_container">{renderItem()}</div>;
};

const MenuItem = withRouter(DashboardItemComponent);
export default MenuItem;
