import React from "react";
import { Dashboard } from "@material-ui/icons";
interface IProps {
  children: any;
}

const Placeholder = ({ children }: IProps) => {
  return (
    <div className="placeholder">
      <div>
        <Dashboard style={{ fontSize: 120 }} />
        <div>{children}</div>
      </div>
    </div>
  );
};

export default Placeholder;
