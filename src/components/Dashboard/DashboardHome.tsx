import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Grid, Avatar } from "@material-ui/core";
import { User } from "../../models/User";

interface IProps {
  users: User[];
  onSetUsers: any;
}

interface IState {
  users: [any];
}

class DashboardHomeComponent extends React.Component<IProps, IState> {
  componentDidMount() {
    const { onSetUsers } = this.props;
    fetch("https://api.github.com/users?since=135")
      .then((res) => res.json())
      .then((users) => {
        onSetUsers(users);
      })
      .catch((err) => {
        console.log("Error");
      });
  }
  render() {
    const { users } = this.props;
    return (
      <Grid container spacing={2}>
        {users.map((user) => (
          <Grid item sm={3}>
            <div className="user_card">
              <Avatar src={user.avatar_url} />
              <p>{user.login}</p>
              <a href={user.html_url}>Visit Github</a>
            </div>
          </Grid>
        ))}
      </Grid>
    );
  }
}

const mapStateToProps = (state: any) => ({
  users: state.userState.users,
});

const mapDispatchToProps = (dispatch: any) => ({
  onSetUsers: (users: User[]) => dispatch({ type: "USERS_SET", users }),
});

const DashboardHome = compose(connect(mapStateToProps, mapDispatchToProps))(
  DashboardHomeComponent
);
export default DashboardHome;
