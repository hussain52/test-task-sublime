import React from "react";
import Placeholder from "./Placeholder";

const Analytics = () => {
  return (
    <div>
      <Placeholder>Analytics</Placeholder>
    </div>
  );
};

export default Analytics;
