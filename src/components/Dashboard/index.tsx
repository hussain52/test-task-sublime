import * as React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { Grid } from "@material-ui/core";
import { withAuthorization } from "../Session/withAuthorization";
import Header from "../blocks/Header";
import Sidebar from "../blocks/Sidebar";
import DashboardItem from "./DashboardItem";

class DashboardComponent extends React.Component<any, any> {
  public render() {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item sm={3}>
            <Sidebar />
          </Grid>
          <Grid item sm={9}>
            <div className="main_container">
              <Header />
              <DashboardItem />
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  users: state.userState.users,
});

const mapDispatchToProps = (dispatch: any) => ({
  onSetUsers: (users: any) => dispatch({ type: "USERS_SET", users }),
});

const authCondition = (authUser: any) => !!authUser;

export const Dashboard = compose(
  withAuthorization(authCondition),
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardComponent);
