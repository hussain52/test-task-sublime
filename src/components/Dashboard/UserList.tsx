import React from "react";
import Placeholder from "./Placeholder";

const UserList = () => {
  return (
    <div>
      <Placeholder>User List</Placeholder>
    </div>
  );
};

export default UserList;
