import * as React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withAuthorization } from "../Session/withAuthorization";
import { Redirect } from "react-router-dom";

const Landing = () => {
  return <Redirect to="/dashboard" />;
};

const mapStateToProps = (state: any) => ({
  authUser: state.sessionState.authUser,
});

const authCondition = (authUser: any) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  connect(mapStateToProps)
)(Landing);
