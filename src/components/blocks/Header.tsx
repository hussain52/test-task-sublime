import React from "react";
import { auth } from "../../firebase";
import { connect } from "react-redux";
import { compose } from "redux";

interface IProps {
  authUser?: any;
}

const HeaderComponent = ({ authUser }: IProps) => {
  return (
    <div className="header">
      <div>Dashboard</div>
      <div className="header_action">
        <div>{authUser.displayName}</div>
        <button onClick={auth.doSignOut}>Logout</button>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  authUser: state.sessionState.authUser,
});

const Header = compose(connect(mapStateToProps, null))(HeaderComponent);
export default Header;
