import React from "react";
import {
  Dashboard,
  Settings,
  AccountCircle,
  AssessmentOutlined,
} from "@material-ui/icons";
import MenuItem from "./MenuItem";

const Sidebar = () => {
  return (
    <div className="sidebar">
      <div className="sidebar_logo">Sublime</div>
      <ul className="sidebar_items">
        <MenuItem name="Dashboard" link="" icon={<Dashboard />} />
        <MenuItem
          name="Settings"
          link="settings"
          icon={<Settings />}
        ></MenuItem>
        <MenuItem name="Users" link="users" icon={<AccountCircle />}></MenuItem>
        <MenuItem
          name="Analytics"
          link="analytics"
          icon={<AssessmentOutlined />}
        ></MenuItem>
      </ul>
    </div>
  );
};

export default Sidebar;
