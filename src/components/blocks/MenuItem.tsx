import React, { Component } from "react";
import classNames from "classnames";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";

interface IProps {
  icon?: any;
  link?: string;
  name?: string;
  match?: any;
}

class MenuItemComponent extends Component<IProps & RouteComponentProps> {
  render() {
    const { name, link, icon, match } = this.props;
    const active = (match.params.subroute || "") === link;
    return (
      <div>
        <li className={classNames({ activeLink: active })}>
          <Link to={`/dashboard/${link}`}>
            <div>{icon}</div>
            <div>{name}</div>
          </Link>
        </li>
      </div>
    );
  }
}

const MenuItem = withRouter(MenuItemComponent);
export default MenuItem;
