import * as React from "react";
import * as routes from "../../constants/routes";
import { CircularProgress } from "@material-ui/core";
import { auth, firebase } from "../../firebase";
import { withRouter, RouteComponentProps } from "react-router-dom";

interface IProps {
  email?: string;
  error?: any;
  history?: any;
  passwordOne?: string;
  passwordTwo?: string;
  username?: string;
}

interface IState {
  email: string;
  error: any;
  passwordOne: string;
  passwordTwo: string;
  username: string;
  progress: boolean;
}

class SignUpForm extends React.Component<IProps & RouteComponentProps, IState> {
  private static INITIAL_STATE = {
    email: "",
    error: null,
    passwordOne: "",
    passwordTwo: "",
    username: "",
    progress: false,
  };

  private static propKey(propertyName: string, value: any): object {
    return { [propertyName]: value };
  }

  constructor(props: IProps & RouteComponentProps) {
    super(props);
    this.state = { ...SignUpForm.INITIAL_STATE };
  }

  public onSubmit(event: any) {
    event.preventDefault();
    const { email, passwordOne, passwordTwo, username } = this.state;
    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === "" ||
      email === "" ||
      username === "";
    const { history } = this.props;
    console.log(history);
    if (!isInvalid) {
      this.setState(SignUpForm.propKey("progress", true));
      auth
        .doCreateUserWithEmailAndPassword(email, passwordOne)
        .then((authUser: any) => {
          firebase.auth.onAuthStateChanged((authUser) => {
            if (authUser) {
              authUser
                .updateProfile({
                  displayName: username,
                })
                .then(() => {
                  history.push(routes.HOME);
                  // Update successful.
                })
                .catch(() => {
                  // An error happened.
                });
            }
          });
          // Create a user in your own accessible Firebase Database too
        })
        .catch((error) => {
          this.setState(SignUpForm.propKey("error", error));
          this.setState(SignUpForm.propKey("progress", false));
        });
    }
  }

  public render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      error,
      progress,
    } = this.state;

    return (
      <form onSubmit={(event) => this.onSubmit(event)}>
        <input
          className="input"
          value={username}
          onChange={(event) => this.setStateWithEvent(event, "username")}
          type="text"
          placeholder="Full Name"
        />
        <input
          className="input"
          value={email}
          onChange={(event) => this.setStateWithEvent(event, "email")}
          type="text"
          placeholder="Email Address"
        />
        <input
          className="input"
          value={passwordOne}
          onChange={(event) => this.setStateWithEvent(event, "passwordOne")}
          type="password"
          placeholder="Password"
        />
        <input
          className="input"
          value={passwordTwo}
          onChange={(event) => this.setStateWithEvent(event, "passwordTwo")}
          type="password"
          placeholder="Confirm Password"
        />
        <button className="button" type="submit">
          {progress ? (
            <CircularProgress style={{ color: "#fff" }} size={20} />
          ) : (
            "Sign Up"
          )}
        </button>
        {error && (
          <p style={{ color: "#d63031", marginTop: 15 }}>{error.message}</p>
        )}
      </form>
    );
  }

  private setStateWithEvent(event: any, columnType: string) {
    this.setState(SignUpForm.propKey(columnType, (event.target as any).value));
  }
}

export default withRouter(SignUpForm);
