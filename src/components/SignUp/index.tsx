import React from "react";
import { Link } from "react-router-dom";
import SignUpForm from "./SingUpForm";
// import { compose } from "redux";
import { withoutAuthorization } from "../Session/withoutAuthorization";

const SignUpComponent = () => {
  return (
    <div>
      <div className="login_overlay"></div>
      <div className="login_container">
        <h2>SignUp</h2>
        <SignUpForm />
        <div className="signup_link">
          <Link to="/signin">Already have an account?</Link>
        </div>
      </div>
    </div>
  );
};

const SignUp = withoutAuthorization(SignUpComponent);
export default SignUp;
