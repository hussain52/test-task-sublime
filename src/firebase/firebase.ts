import * as firebase from "firebase/app";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyCuGKIgkNX9Il-8rf5wrx2vAo9Cc8WCNJE",
  authDomain: "default-demo-app-3044f.firebaseapp.com",
  messagingSenderId: "902856355309",
  projectId: "default-demo-app-3044f",
  storageBucket: "gs://default-demo-app-3044f.appspot.com",
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

export const auth = firebase.auth();
