import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { App } from "./components/App";
import "./index.scss";
import registerServiceWorker from "./registerServiceWorker";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
// import { ConnectedRouter } from "connected-react-router";
import createStore from "./store";

const { store } = createStore();

const theme = createMuiTheme({
  typography: {
    fontFamily: "Averta-Semibold",
  },
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: "#0263c5",
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      light: "#0066ff",
      main: "#0044ff",
      // dark: will be calculated from palette.secondary.main,
      contrastText: "#ffcc00",
    },
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
  },
});

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>,
  document.getElementById("root")
);
registerServiceWorker();
