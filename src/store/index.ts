import { createStore, compose, applyMiddleware } from "redux";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";
import rootReducer from "../reducers";

export default () => {
  // Create a history depending on the environment
  const history = createBrowserHistory();

  const middleware = [routerMiddleware(history)];
  const composedEnhancers = compose(applyMiddleware(...middleware));

  // Create the store
  const store = createStore(
    connectRouter(history)(rootReducer),
    {},
    composedEnhancers
  );

  return {
    store,
    history,
  };
};
